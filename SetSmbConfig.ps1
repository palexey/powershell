﻿$comp=Get-Content "C:\cmd\server_list.txt"
#ForEach($i in $comp) {
#    Echo $i
#    $lp='\\'+$i+'\C$\zabbix'
#    Echo $lp
#    Copy-Item -force -Path C:\zabbix\zabbix_agentd.conf -Destination $lp
#}

ForEach($args in $comp){
    echo $args
    Invoke-Command -ComputerName $args -ScriptBlock {Set-SmbServerConfiguration -EnableSMB1Protocol $false -Force}
}