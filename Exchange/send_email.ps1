﻿#.SYNOPSIS
#Sends SMTP email via the Hub Transport server
#
#.EXAMPLE
#.\Send-Email.ps1 -To "administrator@exchangeserverpro.net" -Subject "Test email" -Body "This is a test"
#

param(
#[string]$to,
#[string]$subject,
#[string]$body
)

$smtpServer = "mail"
$smtpFrom = "administrator@ucpfund.com"
$smtpTo = "clear_download_folders@ucpfund.com"
$messageSubject = "Предупрежение об очистке папки Загрузки"
$messageBody = "Сегодня в 17.15 на Вашем компьютере будет очищена папка Загрузки (Download). Если Вам необходимы какие-либо файлы в указанной папке, перенесите их в другое место. Данное сообщение отправлено роботом и ответа на него не требуется."

$smtp = New-Object Net.Mail.SmtpClient($smtpServer)
$smtp.Send($smtpFrom,$smtpTo,$messagesubject,$messagebody)