﻿$Mbx = Get-CASMailbox -Filter {HasActiveSyncDevicePartnership -Eq $true -And ServerName -eq "mx"}

ForEach ($MB in $Mbx)

{

Get-MobileDeviceStatistics -Mailbox $MB.Identity | Select-Object Identity, Lastsyncattempttime, LastSuccessSync, Devicetype, DeviceOS, DeviceFriendlyname, Status, DeviceAccessState | Export-CSV “c:\files\mobile.csv” -NoTypeInformation –Append

}