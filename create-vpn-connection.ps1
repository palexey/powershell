﻿Add-VpnConnection -Name "vpn.euroexpo.ru" `
  -ServerAddress "vpn.euroexpo.ru" `
  -TunnelType IKEv2 `
  -EncryptionLevel Maximum `
  -AuthenticationMethod EAP `
  -RememberCredential

Set-VpnConnectionIPsecConfiguration -ConnectionName "vpn.euroexpo.ru" `
  -AuthenticationTransformConstants GCMAES256 `
  -CipherTransformConstants GCMAES256 `
  -EncryptionMethod GCMAES256 `
  -IntegrityCheckMethod SHA384 `
  -DHGroup ECP384 `
  -PfsGroup ECP384 `
  -Force