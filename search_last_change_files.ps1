$targetDate = Get-Date -Year 2024 -Month 2 -Day 21
$files = Get-ChildItem -Recurse | Where-Object { $_.LastWriteTime.Date -eq $targetDate.Date }
$table = @()
foreach ($file in $files) {
    $row = New-Object -TypeName PSObject
    $row | Add-Member -MemberType NoteProperty -Name "File Name" -Value $file.Name
    $row | Add-Member -MemberType NoteProperty -Name "Path" -Value $file.FullName
    $row | Add-Member -MemberType NoteProperty -Name "Last Modified" -Value $file.LastWriteTime
    $table += $row
}
$table | Format-Table
