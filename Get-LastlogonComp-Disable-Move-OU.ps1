﻿$date_with_offset= (Get-Date).AddDays(-120)
$comps = Get-ADComputer -Properties LastLogonDate -Filter {(LastLogonDate -lt $date_with_offset) -and (Enable -eq 'True')} | Sort LastLogonDate
foreach ($comp in $comps) 
    {set-adcomputer $comp.name -enabled $false
    get-ADComputer $comp |move-adobject -targetpath "OU=Disabled,OU=PC,DC=UE161,DC=RU"} 
Get-ADComputer -Properties LastLogonDate -Filter {(LastLogonDate -lt $date_with_offset) -and (Enable -eq 'True')} | Sort LastLogonDate | FT Name, LastLogonDate -AutoSize | Out-File c:\tmp\moved_disable_pc.txt