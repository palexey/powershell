﻿$compArray = (Get-ADComputer -Filter {Enabled -eq "True" -and OperatingSystem -Like '*Windows Server*'}).Name
$VerbosePreference = "continue"
#$list = (Get-ADComputer -LDAPFilter "(&(objectcategory=computer)(OperatingSystem=*server*))").Nam

foreach($strComputer in $compArray) {
Get-WMIObject Win32_Service -ComputerName $strComputer | Where-Object{$_.StartName -ne 'LocalSystem'} |Where-Object{$_.StartName -ne 'NT Authority\NetworkService'} |Where-Object{$_.StartName -ne 'NT AUTHORITY\LocalService'} | Where-Object{$_.StartName -ne 'NT SERVICE\ksnproxy'} |Sort-Object -Property StartName | Format-Table SystemName, Name, StartName |Out-File -FilePath C:\files\newyol\service_log_ass_$($strComputer).txt
}
