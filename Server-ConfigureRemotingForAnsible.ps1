﻿$compArray = (Get-ADComputer -Filter {Enabled -eq "True" -and OperatingSystem -Like '*Windows Server*'}).Name
$VerbosePreference = "continue"
#$list = (Get-ADComputer -LDAPFilter "(&(objectcategory=computer)(OperatingSystem=*server*))").Nam

foreach($strComputer in $compArray) {
    Invoke-Command -ComputerName $strComputer -FilePath .\ConfigureRemotingForAnsible.ps1 
}
