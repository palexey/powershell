﻿$date_with_offset= (Get-Date).AddDays(-120)
Get-ADComputer -Properties LastLogonDate -Filter {LastLogonDate -lt $date_with_offset } -SearchBase "CN=Computers,DC=ue161,DC=ru"|ft Name, distinguishedName, LastLogonDate
#Get-ADComputer -Properties LastLogonDate -Filter {LastLogonDate -lt $date_with_offset } -SearchBase "OU=PC,DC=ue161,DC=ru"|ft Name, LastLogonDate