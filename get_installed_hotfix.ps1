$compArray = (Get-ADComputer -Filter {Enabled -eq "True" -and OperatingSystem -Like '*Windows Server*'}).Name
$VerbosePreference = "continue"

foreach($strComputer in $compArray) {
    Get-HotFix -ComputerName $strComputer | Sort-Object InstalledOn -Descending |Select -First 5 >> c:\files\installed_hotfix.txt
}