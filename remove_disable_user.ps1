﻿# removeDisabledUsers.ps1
# Written by Ethan Dodge (ethangd1) on 5/28/2014
# This script removes all disabled users from all security and distribution groups in the specified "searchOU"

# Uncomment the next line if you're not running this script from RSAT
# Import-Module ActiveDirectory

$searchOU="OU=Users,DC=idms-finance,DC=local"

Get-ADGroup -Filter 'GroupCategory -eq "Security" -or GroupCategory -eq "Distribution"' -SearchBase $searchOU | ForEach-Object{ $group = $_
	Get-ADGroupMember -Identity $group -Recursive | %{Get-ADUser -Identity $_.distinguishedName -Properties Enabled | ?{$_.Enabled -eq $false}} | ForEach-Object{ $user = $_
		$uname = $user.Name
		$gname = $group.Name
		Write-Host "Removing $uname from $gname" -Foreground Yellow
		Remove-ADGroupMember -Identity $group -Member $user -Confirm:$false
	}
}