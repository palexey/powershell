﻿Import-Module -Name ActiveDirectory

# read AD security from NewOU1
$sd = Get-Acl -Path 'AD:\CN=Перов Алексей Владимирович,OU=НьюЙол,OU=ue161,DC=UE161,DC=RU'

# assign security to NewOU2
Set-Acl -Path 'AD:CN=Zabbix,OU=Service,OU=IT,OU=Наследие,OU=Отключенные,DC=UE161,DC=RU' -AclObject $sd 
