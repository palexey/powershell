﻿#cls

# Задаем путь к корневой папке
$RootPath = "C:\Files\Share\"

# Определяем полный путь к папке с запущенным скриптом
$ScriptPath = $MyInvocation.MyCommand.Definition | `
split-path -parent
# Определяем имя запущенного скрипта
$ScriptName = $MyInvocation.MyCommand.Name
# Вырезаем ".ps1" из имени скрипта
$ScriptName = $ScriptName.Replace(".ps1","")
# Задаем полный путь log-файла
$LogPath = $ScriptPath + "\" + $ScriptName + ".log"

# Запрашиваем текущее точное время
$dt = get-date -Format "yyyy-MM-d_HH-mm-ss"
# Задаем путь csv-файла
$CsvPath = $ScriptPath + "\" + $ScriptName + " (" + $dt + `
")" + ".csv"

# Функция записи в лог передаваемой строки $str
Function Write-Log ($str) {
  # Запрашиваем текущее точное время
  $dt = get-date -Format "d.MM.yyyy HH:mm:ss"
  # Формируем строку с текущим временем
  $str = "[" + $dt + "]" + " " + $str
  # Пишем строку в лог
  $str | Out-File -FilePath $LogPath -Append -Encoding UTF8
}

# Если корневая папка не существует
if (!(Test-Path $RootPath)) {
  # Выводим на экран сообщение с ошибкой
  Write-Host
  Write-Host "Указан неверный путь к папке!"
  Write-Host
  # Записываем сообщение с ошибкой в лог
  Write-Log "Указан неверный путь к папке!"
  # И прерываем скрипт
  exit
}

# Запрашиваем атрибуты корневой папки
$dir = Get-Item $RootPath | select FullName, CreationTime, `
LastAccessTime, LastWriteTime
# Запрашиваем списки доступа корневой папки
$acls = Get-Acl $RootPath | select -expand access | `
where {$_.IdentityReference -notmatch "BUILTIN|NT AUTHORITY|`
EVERYONE|CREATOR OWNER|ВЛАДЕЛЕЦ"}

# Создаем новый PSObject, "суммирующий" свойства $dir и $acl
$Obj = new-object PSObject
$Obj | add-member -membertype NoteProperty -name `
"FullName" -Value ""
$Obj | add-member -membertype NoteProperty -name `
"CreationTime" -Value ""
$Obj | add-member -membertype NoteProperty -name `
"LastAccessTime" -Value ""
$Obj | add-member -membertype NoteProperty -name `
"LastWriteTime" -Value ""
$Obj | add-member -membertype NoteProperty -name `
"FileSystemRights" -Value ""
$Obj | add-member -membertype NoteProperty -name `
"AccessControlType" -Value ""
$Obj | add-member -membertype NoteProperty -name `
"IdentityReference" -Value ""
$Obj | add-member -membertype NoteProperty -name `
"IsInherited" -Value ""
$Obj | add-member -membertype NoteProperty -name `
"InheritanceFlags" -Value ""
$Obj | add-member -membertype NoteProperty -name `
"PropagationFlags" -Value ""

# Функция "склеивания" свойств объектов папки $dir и
# списков доступа к папке $acl в новом объекте PSObject - $Obj
Function Init-Obj ($dir, $acl) {
  # Задаем свойства $Obj значениями свойств $dir
  $Obj.FullName = $dir.FullName
  $Obj.CreationTime = $dir.CreationTime.ToString()
  $Obj.LastAccessTime = $dir.LastAccessTime.ToString()
  $Obj.LastWriteTime = $dir.LastWriteTime.ToString()
  # Задаем свойства $Obj значениями списка доступа $acl
  $Obj.FileSystemRights = $acl.FileSystemRights
  $Obj.AccessControlType = $acl.AccessControlType
  $Obj.IdentityReference = $acl.IdentityReference.Value
  $Obj.IsInherited = $acl.IsInherited
  $Obj.InheritanceFlags = $acl.InheritanceFlags
  $Obj.PropagationFlags = $acl.PropagationFlags
}

# Перебираем все списки доступа корневой папки $dir
foreach ($acl in $acls) {
  # Задаем значения $Obj из свойств $dir и $acl
  Init-Obj $dir $acl
  # Записываем полученные значения в csv-файл
  $Obj | Export-csv $CsvPath -Append -Encoding utf8 `
  -Force -NoTypeInformation -Delimiter ";"
}

# Рекурсивная функция перебора дерева подкаталогов
# корневой папки $dir
Function Recur ($dir) {
  # Находим подкаталоги в корневой папке
  $childs = Get-ChildItem $dir.FullName -Directory
  # Перебираем подкаталоги
  foreach ($child in $childs) {
    # Поиск ненаследованных прав доступа папки $child
    $acls = Get-Acl $child.FullName | select -expand access | `
    where {($_.IdentityReference -notmatch "BUILTIN|NT AUTHORITY|`
    EVERYONE|CREATOR OWNER|ВЛАДЕЛЕЦ") -and ($_.IsInherited `
    -eq $false)}
    # Если есть ненаследованные права доступа
    if ($acls.count -ne 0) {
      # Перебираем списки доступа к папке $child
      foreach ($acl in $acls) {
        # Формируем $Obj из свойств $child и $acl
        Init-Obj $child $acl
        # Записываем полученные значения в csv-файл
        $Obj | Export-csv $CsvPath -Append -Encoding utf8 -Force `
        -NoTypeInformation -Delimiter ";"
      }
    }
    # Если у подкаталога есть дочерние директории
    if ($child.GetDirectories().Count -ne 0) {
      # Падаем в рекурсию, перебирая дочерние директории
      Recur $child
    }
  }
}

# Перебор дерева подкаталогов корневой папки $dir
Recur $dir