﻿function Get-OpenFile
{
    Param
    (
        [string]$ComputerName
    )

    $openfiles = openfiles.exe /query /s $computerName /fo csv /V

    $openfiles | ForEach-Object {

        $line = $_

        if ($line -match '","')
        {
            $line
        }

    } | ConvertFrom-Csv
}

Get-OpenFile -ComputerName dc01-ru