﻿$Date = Get-Date -f "yyyy-MM-dd"
$Time = Get-Date -f "HH-mm-ss"

$Computers = (Get-ADComputer -Filter {Enabled -eq "True" } -LDAPFilter "(&(objectcategory=computer)(OperatingSystem=*server*))").name
$Computers > C:\Tmp\servers_"$Date"_"$Time".txt
$ErrorActionPreference = "SilentlyContinue"
$Report = @()

##
$valueToUsers = @(
    'S-1-5-9',
    'S-1-5-18',
    'S-1-5-19' 
)
##

foreach ($Computer in $Computers)
{
    if (test-connection $Computer -quiet -count 1)
    {   
            
        #Computer is online
        $path = "\\" + $Computer + "\c$\Windows\System32\Tasks"
        $tasks = Get-ChildItem -recurse -Path $path -File
        foreach ($task in $tasks)
        {
            $Info = Get-ScheduledTask -CimSession $Computer -TaskName $task |
                Where-Object {$_.State -like 'R*'} |
                    Where-Object {$_.Application -ne $null} |
                        Get-ScheduledTaskInfo |
                            Select LastRunTime, LastTaskResult
    
            $Details = "" |Where-Object {($_.User -ne 'S-1-5-9') -or ($_.User -ne 'S-1-5-18') -or ($_.User -ne 'S-1-5-19') -or ($_.User -ne $null) -or ($_.User -ne 'SYSTEM')} |
                 select ComputerName, Task, User, Enabled, Application, LastRunTime, LastTaskResult
            $AbsolutePath = $task.directory.fullname + "\" + $task.Name
            $TaskInfo = [xml](Get-Content $AbsolutePath)
            $Details.ComputerName = $Computer
            $Details.Task = $task.name
            $Details.User = $TaskInfo.task.principals.principal.userid
            $Details.Enabled = $TaskInfo.task.settings.enabled
            $Details.Application = $TaskInfo.task.actions.exec.command
            $Details.LastRunTime = $Info.LastRunTime
            $Details.LastTaskResult = $Info.LastTaskResult
            $Details
            $Report += $Details
        }
    }
    else
    {
        #Computer is offline
    }
}
$Report | Export-csv C:\Tmp\Tasks_"$Date"_"$Time".csv -NoTypeInformation