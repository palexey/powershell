$ftpServer = 'server'
$ftpUsername = 'user'
$ftpPassword = ConvertTo-SecureString 'password' -AsPlainText -Force
$localPath = "path"
####$yesterday = (Get-Date).AddDays(-1).ToString("yyyyMMdd")
###Write-Output $yesterday
$remotePath = "/file"
$cred = New-Object system.Management.Automation.PSCredential($ftpUsername,$ftpPassword)

Set-FTPConnection -Credentials $cred -Server $ftpServer -Session serv -EnableSsl -ignoreCert -UsePassive

Get-FTPItem -Path $remotePath -LocalPath $localPath -Session $serv -Overwrite

$today = Get-Date
$yesterday = $today.AddDays(-1)
$pattern = "/.*?/$($yesterday.ToString("yyyyMMdd"))/"
$filters = @("env1", "en2", "env4")

$remoteFiles=Get-Content -Path "path" |Select-String -pattern $filters -AllMatches | Where-Object { $_ -match $pattern }
foreach ($newRemotefiles in $remoteFiles)  {
    Get-FTPItem -Path $newRemoteFiles -LocalPath $localPath -Session $serv
}
